# Rytmika

Bartłomiej Bolek nr 287048

Projekt został zrealizowany według przedstawionych przez Pana Doktora wytycznych. Z góry przepraszam za długość funkcji czytajacej rytmy z pliku (rozbicie jej byłoby możliwe, jednak byłby to sztuczny zabieg nie poprawiajacy za bardzo jej czytelności)

Funkcjonalność przetestowałem na pomocą skryptów w Pythonie i z dokładnością do szybkości interpretera udało mi się osiągnąc zamierzony rezultat.

Otrzymywane sygnały są bufferowane w self-pipie, co ma zagwarantować nie gubienie ich. 
Komentarze w pliku znajdują się pomiędzy dwoma znakami '/'. Są wielolinijkowe, komentują wtedy one również nową linie.
Sprawdzanie dostępności kolejki FIFO zrobione jest aktywnym przechodzeniem listy i sprawdzaniem statusu plików. W celu odciążenia procesora użyty został nanosleep 0.1 ms.
Asynchroniczność osiągnięta została za pomocą tricku polegajacego na segregowaniu "stanu wykonywania rytmu" względem ilości czasu pozostałego do następnej operacji. Wykonywany zostaje nanosleep z tym czasem i od drugiego stanu (jeżeli taki istnieje) odejmowany jest ten czas. Uwzględniony jest też przypadek przyjścia sygnału w trakcie nanosleep. 

Mam nadzieję, że kod jest wystarczająco czytelny. W razie uwag prosiłbym o kontakt.

PS: Forking ostatecznie nie został użyty.