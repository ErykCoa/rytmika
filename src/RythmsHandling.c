//
// Created by student on 09.01.18.
//

#include "../inc/RythmsHandling.h"
#include "../inc/Structures.h"

void loadRythmsList(CyclicRythmList ** rythmsList, char* filePath){
    int file = open(filePath, O_RDONLY);

    if (file < 0)
        criticalError("Could not open config file!");

    char  buf[64];
    int itemCount = 0;
    size_t currentSize = 0;
    int * currentLengths = 0;
    ssize_t readRes = 0;
    CyclicRythmList * currentElement = 0;
    CyclicRythmList * lastElement = 0;
    int commentStarted = 0;
    int asteriskCount = 0;
    int dotCount = 1;


    while (1){
        if ((readRes = read(file, &buf, 64)) < 0)
            criticalError("Error while reading config file!");

        if (!readRes)
            break;

        for(int i = 0; i < readRes; ++i){
            if (!currentElement) {
                currentElement = calloc(1, sizeof(CyclicRythmList));

                if (!currentElement)
                    criticalError("Malloc has failed!");

                currentElement->data = calloc(1, sizeof(Rythm));

                if (!currentElement->data)
                    criticalError("Malloc has failed!");

                if (lastElement)
                    lastElement->next = currentElement;
                else
                    *rythmsList = currentElement;
            }

            if (itemCount == currentSize) {

                currentLengths = realloc(currentLengths, sizeof(int)*(currentSize + 5));

                if (!currentLengths)
                    criticalError("Malloc has failed!");

                currentSize += 5;
            }

            if (buf[i] == '/'){
                if (commentStarted){

                    commentStarted = 0;
                } else {
                    commentStarted = 1;
                }
            } else if (!commentStarted) {
                switch (buf[i]) {
                    case '\n':

                        if (dotCount){
                            currentLengths[itemCount++] = dotCount;
                        }

                        if (asteriskCount){
                            currentLengths[itemCount++] = asteriskCount;
                        }

                        currentElement->data->signalsAndPausesLengths = currentLengths;
                        currentElement->data->size = itemCount;

                        lastElement = currentElement;
                        currentElement = 0;
                        currentSize = 0;
                        currentLengths = 0;
                        itemCount = 0;
                        asteriskCount = 0;
                        dotCount = 1;

                        break;
                    case '*':

                        if (dotCount){
                            currentLengths[itemCount++] = dotCount;
                            dotCount = 0;
                        }

                        asteriskCount++;

                        break;
                    case '.':

                        if (asteriskCount){
                            currentLengths[itemCount++] = asteriskCount;
                            asteriskCount = 0;
                        }

                        dotCount++;
                        break;

                    case ' ':
                    case '\t':
                        break;

                    default:
                        criticalError("Config file is corrupted!");
                }
            }
        }
    }

    if (currentElement){
        if (dotCount){
            currentLengths[itemCount++] = dotCount;
        }

        if (asteriskCount){
            currentLengths[itemCount++] = asteriskCount;
        }
        currentElement->data->signalsAndPausesLengths = currentLengths;
        currentElement->data->size = itemCount;
        currentElement->next = *rythmsList;
    } else if (lastElement){
        lastElement->next = *rythmsList;
    }

}

void sendRythms(int * file, Arguments * arguments, RythmStatus ** rythmsStatus){

    if (!rythmsStatus[0])
        return;

    int rythmsCount = 2 - !rythmsStatus[1];

    if (rythmsCount > 2)
        criticalError("Only 2 rythms are supported ATM!");

    if (rythmsCount == 2 && rythmsStatus[0]->delayLeft > rythmsStatus[1]->delayLeft){
        RythmStatus * tmpRythmStatus = rythmsStatus[0];
        rythmsStatus[0] = rythmsStatus[1];
        rythmsStatus[1] = tmpRythmStatus;
    }

    struct timespec req = {};
    struct timespec rest = {};

    float delay = (*rythmsStatus)->delayLeft;

    req.tv_sec = (long) delay;
    req.tv_nsec =  (long) ((delay - (long)delay) * 1000000000L);

    if (nanosleep(&req, &rest)){
        delay -= rest.tv_sec + rest.tv_nsec / 1000000000.f;
        (*rythmsStatus)->delayLeft -= delay;
    } else {

        if ((*rythmsStatus)->currentIndex % 2 && write(*file, &rythmsStatus[0]->signalType, 1) == -1){
            *file = getNextValidFIFO(arguments);
            return;
        }

        (*rythmsStatus)->delayLeft = arguments->delay;

        if (++(*rythmsStatus)->currentSubIndex >= (*rythmsStatus)->rythm->signalsAndPausesLengths[(*rythmsStatus)->currentIndex]){

            (*rythmsStatus)->currentSubIndex = 0;
            if (++(*rythmsStatus)->currentIndex >= (*rythmsStatus)->rythm->size){

                if (rythmsCount == 2) {
                    rythmsStatus[1]->delayLeft -= delay;
                }

                free(*rythmsStatus);
                *rythmsStatus = rythmsStatus[1];
                rythmsStatus[1] = NULL;
                return;
            }
        }
    }

    if (rythmsCount == 2){
        rythmsStatus[1]->delayLeft -= delay;
    }
}

int hasSlot(RythmStatus ** rythmsStatus, int sigNum){
    return !rythmsStatus[1] && (!rythmsStatus[0] || rythmsStatus[0]->sigNum != sigNum);
}

void insertRythm(RythmStatus ** rythmsStatus, CyclicRythmList ** rythmsList, Arguments * arguments, char * signalType, int sigNum){
    RythmStatus * res = calloc(1, sizeof(RythmStatus));

    res->delayLeft = arguments->delay;
    res->sigNum = sigNum;
    res->signalType = *signalType;
    res->rythm = (*rythmsList)->data;
    *rythmsList = (*rythmsList)->next;

    if (!rythmsStatus[0]){
        rythmsStatus[0] = res;
    } else {
        rythmsStatus[1] = res;
    }

    if (*signalType == 'Z') {
        *signalType = 'A';
    } else {
        *signalType = (char) (*signalType + 1);
    }
}