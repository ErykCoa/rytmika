//
// Created by student on 09.01.18.
//

#include "../inc/Forking.h"
#include "../inc/Structures.h"

int createForks(ChildPIDs * PIDs){

    for (int i = 0; i < 2; ++i){
        PIDs->childPID[i] = fork();

        if(PIDs->childPID[i] >= 0)
        {
            if(PIDs->childPID[i] == 0)
            {
                return 0;
            }
        }
        else
            criticalError("Could not create fork!");
    }

    return 1;
}
