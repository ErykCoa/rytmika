//
// Created by student on 09.01.18.
//

#include "../inc/SignalHandling.h"

void setHandler(int sig, void (*handler)(int)){
    struct sigaction sa;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_RESTART;
    sa.sa_handler = handler;
    if (sigaction(sig, &sa, NULL) == -1)
        criticalError("sigaction");
}


