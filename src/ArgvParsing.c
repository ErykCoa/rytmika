//
// Created by student on 08.01.18.
//

#include "../inc/ArgvParsing.h"


static struct option long_options[] =
        {
                {"delay", required_argument, 0, 'd'},
                {0, 0, 0, 0}
        };

Arguments * getArguments(int argc, char **argv){

    Arguments * arguments = calloc(1, sizeof(Arguments));

    if (!arguments)
        criticalError("Malloc has failed!");

    int c;

    while (1)
    {
        c = getopt_long(argc, argv, "d:", long_options, 0);

        if (c == -1)
            break;

        switch (c)
        {
            case 'd':
            case 0:
                if (!optarg)
                    criticalError("delay value is required!");

                arguments->delay = strtof(optarg, 0);
                break;
            case '?':
                break;
            default:
                abort();
        }
    }

    if (optind < argc)
    {
        arguments->files = calloc((size_t)argc - optind, sizeof(CyclicList));

        if (!arguments->files)
            criticalError("Malloc has failed!");

        for (int i = 0; i < argc - optind; ++i){
            arguments->files[i].data = argv[optind + i];

            if (i + 1 < argc - optind)
                arguments->files[i].next = arguments->files + i + 1;
            else
                arguments->files[i].next = arguments->files;
        }
    }
    else
        criticalError("At least one file is required!");

    validateArguments(arguments);

    return arguments;
}

void validateArguments(Arguments * arguments){

    if (arguments->delay <= 0)
        criticalError("Delay is required and must be > 0!");
}