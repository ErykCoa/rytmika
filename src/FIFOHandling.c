//
// Created by student on 09.01.18.
//

#include <fcntl.h>
#include "../inc/FIFOHandling.h"
#include "../inc/Structures.h"
#include <sys/stat.h>
#include <time.h>
#include <asm/errno.h>
#include <errno.h>


int getNextValidFIFO(Arguments * arguments){
    int res;
    struct stat st;
    struct timespec req;
    struct timespec rest;

    req.tv_sec = 0;
    req.tv_nsec = 100000L; // 0.1 of millisecond

    while (1){
        res = open(arguments->files->data, O_WRONLY);

        if (res >= 0 && !fstat(res, &st) && S_ISFIFO(st.st_mode))
            return res;

        arguments->files = arguments->files->next;

        nanosleep(&req, &rest);
    }
}

void initSelfPipe(int * pfd){

    if (pipe(pfd) == -1)
        criticalError("Could not create Pipe!");

    if (fcntl(pfd[0], F_GETFL) == -1)
        criticalError("fcntl-F_GETFL");

    if (fcntl(pfd[0], F_SETFL, O_NONBLOCK) == -1)
        criticalError("fcntl-F_SETFL");

    if (fcntl(pfd[1], F_GETFL) == -1)
        criticalError("fcntl-F_GETFL");

    if (fcntl(pfd[1], F_SETFL, O_NONBLOCK) == -1)
        criticalError("fcntl-F_SETFL");
}
