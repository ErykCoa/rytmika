//
// Created by student on 09.01.18.
//

#ifndef RYTMIKA_SIGNALHANDLING_H
#define RYTMIKA_SIGNALHANDLING_H

#include "ErrorHandling.h"
#include <signal.h>

void setHandler(int sig, void (*handler)(int));

#endif //RYTMIKA_SIGNALHANDLING_H
