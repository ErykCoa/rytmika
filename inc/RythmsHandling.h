//
// Created by student on 09.01.18.
//

#ifndef RYTMIKA_RYTHMSHANDLING_H
#define RYTMIKA_RYTHMSHANDLING_H

#include <fcntl.h>
#include "Structures.h"
#include "FIFOHandling.h"
#include <sys/stat.h>
#include <time.h>
#include <asm/errno.h>
#include <errno.h>

void loadRythmsList(CyclicRythmList ** rythmsList, char* filePath);
void sendRythms(int * file, Arguments * arguments, RythmStatus ** rythmsStatus);
int hasSlot(RythmStatus ** rythmsStatus, int sigNum);
void insertRythm(RythmStatus ** rythmsStatus, CyclicRythmList ** rythmsList, Arguments * arguments, char * signalType, int sigNum);

#endif //RYTMIKA_RYTHMSHANDLING_H
