//
// Created by student on 09.01.18.
//

#ifndef RYTMIKA_FORKING_H
#define RYTMIKA_FORKING_H

#include "Structures.h"
#include <unistd.h>
#include "ErrorHandling.h"

int createForks(ChildPIDs * PIDs);

#endif //RYTMIKA_FORKING_H
