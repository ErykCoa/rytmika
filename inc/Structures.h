//
// Created by student on 08.01.18.
//

#ifndef RYTMIKA_STRUCTURES_H
#define RYTMIKA_STRUCTURES_H

#include <unistd.h>

typedef struct CyclicList CyclicList;

struct CyclicList {
    char* data;
    CyclicList * next;
};

typedef struct {
    float delay;
    CyclicList * files;
} Arguments;

typedef struct {
    pid_t childPID[2];
} ChildPIDs;

typedef struct {
    int size;
    int * signalsAndPausesLengths;
} Rythm;

typedef struct {
    int sigNum;
    char signalType;
    int currentIndex;
    int currentSubIndex;
    float delayLeft;
    Rythm * rythm;
} RythmStatus;

typedef struct CyclicRythmList CyclicRythmList;

struct CyclicRythmList {
    Rythm* data;
    CyclicRythmList * next;
};

#endif //RYTMIKA_STRUCTURES_H
