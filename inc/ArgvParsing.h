//
// Created by student on 08.01.18.
//

#ifndef RYTMIKA_ARGVPARSING_H
#define RYTMIKA_ARGVPARSING_H

#include <stdio.h>
#include <stdlib.h>
#include <getopt.h>
#include "ErrorHandling.h"
#include <string.h>
#include "Structures.h"

Arguments * getArguments(int argc, char **argv);
void validateArguments(Arguments * arguments);

#endif //RYTMIKA_ARGVPARSING_H
