//
// Created by student on 08.01.18.
//

#ifndef RYTMIKA_ERRORHANDLING_H
#define RYTMIKA_ERRORHANDLING_H

#include <stdio.h>
#include <stdlib.h>

void criticalError(char * msg);

#endif //RYTMIKA_ERRORHANDLING_H
