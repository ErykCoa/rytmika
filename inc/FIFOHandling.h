//
// Created by student on 09.01.18.
//

#ifndef RYTMIKA_FIFOHANDLING_H
#define RYTMIKA_FIFOHANDLING_H

#include "Structures.h"
#include "ErrorHandling.h"

int getNextValidFIFO(Arguments * arguments);
void initSelfPipe(int * pfd);

#endif //RYTMIKA_FIFOHANDLING_H
