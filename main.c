
#include "inc/ArgvParsing.h"
#include "inc/Structures.h"
#include "inc/FIFOHandling.h"
#include "inc/Forking.h"
#include <sys/time.h>
#include <sys/select.h>
#include <fcntl.h>
#include "inc/SignalHandling.h"
#include <unistd.h>
#include "inc/ErrorHandling.h"
#include <errno.h>
#include <sys/stat.h>
#include <time.h>
#include "inc/RythmsHandling.h"

static int pipeFileDescriptors[2];


static void handler(int sig) {
    int savedErrno = errno;

    switch (sig){
        case SIGUSR1:
            if (write(pipeFileDescriptors[1], "1", 1) == -1 && errno != EAGAIN)
                criticalError("Could not write 1 to pipe!");
            break;
        case SIGUSR2:
            if (write(pipeFileDescriptors[1], "2", 1) == -1 && errno != EAGAIN)
                criticalError("Could not write 2 to pipe!");
            break;
        default:
            break;
    }

    errno = savedErrno;
}

int main (int argc, char **argv) {
    Arguments * arguments = getArguments(argc, argv);

    int currentFile = -1;

    initSelfPipe(pipeFileDescriptors);
    setHandler(SIGUSR1, handler);
    setHandler(SIGUSR2, handler);

    int selectStatus;
    char pipeChar;

    fd_set pipeFileDescriptor;
    FD_ZERO(&pipeFileDescriptor);
    FD_SET(pipeFileDescriptors[0], &pipeFileDescriptor);

    int char1Count = 0;
    int char2Count = 0;

    RythmStatus * rythmsStatus[2] = {0, 0};
    CyclicRythmList * rythmsList = 0;
    char signalType = 'A';


    loadRythmsList(&rythmsList, "rytmika.conf");

    while (1) {

        while ((selectStatus = select(pipeFileDescriptors[0] + 1, &pipeFileDescriptor, NULL, NULL, 0)) == -1 &&
               errno == EINTR)
            continue;

        if (selectStatus == -1)
            criticalError("Unexpected error while using select!");

        do {

            while (1) {
                if (read(pipeFileDescriptors[0], &pipeChar, 1) == -1) {
                    if (errno == EAGAIN)
                        break;
                    else
                        criticalError("Unexpected error while reading pipe!");
                }

                switch (pipeChar) {
                    case '1':
                        char1Count++;
                        break;
                    case '2':
                        char2Count++;
                        break;
                    default:
                        break;
                }
            }

            if (char1Count && hasSlot(rythmsStatus, 1)) {
                insertRythm(rythmsStatus, &rythmsList, arguments, &signalType, 1);
                char1Count--;
            }

            if (char2Count && hasSlot(rythmsStatus, 2)) {
                insertRythm(rythmsStatus, &rythmsList, arguments, &signalType, 2);
                char2Count--;
            }

            sendRythms(&currentFile, arguments, rythmsStatus);

        } while (rythmsStatus[0] || rythmsStatus[1] || char1Count || char2Count);
    }
}